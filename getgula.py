import csv
import requests
from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.error import HTTPError
from urllib.error import URLError


with open('gula.csv') as csvarchivo:
    entrada = csv.reader(csvarchivo)
    archivo = open('gula.txt','a')
    for registro in entrada:
        try:

            regToStr = ' '.join([str(elem) for elem in registro]) 
            req = requests.get(regToStr)

        except HTTPError as e:

            print(e)

        except URLError:

            print("Server down or incorrect domain")

        else:                
                soup = BeautifulSoup(req.text, "lxml")
                cuerpo = soup.find( class_ = "post-body entry-content")
                cuerpo1 = cuerpo.getText()        
                archivo.write(cuerpo1)
    archivo.close
