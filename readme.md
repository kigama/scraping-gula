# scraping-gula

Este proyecto consiste en realizar scraping a la novela

## Comenzando 🚀

_Es necesario tener python 3 instalado_

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Python 3_
_Python 3_

```
import urllib.request
import sys
import csv
import requests
from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.error import HTTPError
from urllib.error import URLError
```
_Clonar el proyecto_
### Ejecutar sraping 🔧

_Al clonar el proyecto ejecuta en la terminal:_



```
python3 gula.py
```

_Analiza el index general de las publicaciones_

```
https://animeshoy12.blogspot.com/2020/03/the-second-coming-of-gluttony-novela.html
...
https://animeshoy12.blogspot.com/2020/03/the-second-coming-of-gluttony-novelaxx.html
```

_Copia a mano este indice en un archivo llamado gula.csv_

_Ejecuta en a terminal el siguiente comando en la terminal_

```
python3 getgula.py
```
_Todo el sraping obtenido esta guardado en el archivo gula.txt_

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **kigama** - *Trabajo Inicial* - [kigama](https://gitlag.com/kigama)


